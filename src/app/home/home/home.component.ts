import { Component, OnInit } from '@angular/core';
import { UtilService } from '../../services/local/util.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  people;
  votes1 = false;
  constructor( public util: UtilService, private router: Router) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    const data = localStorage.getItem('people');
    if ( data == null ) {
      this.util.saveData();
      window.location.reload();
    } else {
      this.people = JSON.parse(data);
    }
  }
  vote( id, type ) {
    console.log(id);
    let like = 0;
    let dislike = 0;
    let totalVotes = 0;
    let percentLike = 0;
    let percentDisLike = 0;
    // tslint:disable-next-line: prefer-for-of
    for ( let i = 0; i < this.people.length; i++ ) {
      if ( this.people[i].id === id ) {
        console.log(this.people[i].id);
        like = this.people[i].like;
        dislike = this.people[i].dislike;
        if ( type === 'like') {
          like = like + 1;
          this.people[i].like = like;
        }
        if ( type === 'dislike') {
          dislike = dislike + 1;
          this.people[i].dislike = dislike;
        }

        totalVotes = like + dislike;
        percentLike = (like * 100) / totalVotes;
        percentDisLike = 100 - percentLike;

        this.people[i].percent_like = percentLike;
        this.people[i].percent_dislike = percentDisLike;
      }
    }
    if ( id === 1) {
      this.votes1 = true;
    }
    console.log(this.people);
    this.util.updateData(this.people);
  }
}

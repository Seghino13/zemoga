import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor() { }

  getData(){
    localStorage.getItem('people');
  }

  saveData() {
    const people = [
      { id: 1,
        title: 'Pope Francis?',
        description: 'He’s talking tough on clergy sexual abuse, but is he just another papal pervert protector? (thumbs down) or a true pedophile punishing pontiff? (thumbs up)', 
        like: 2,
        dislike: 1,
        percent_like: 50,
        percent_dislike: 50, featured: 1},
      { id: 2,
        title: 'Kanye West',
        description: 'Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.', 
        like: 1,
        dislike: 1,
        percent_like: 50,
        percent_dislike: 50,
        img: '../assets/imgs/k-west.png', featured: 0},
      { id: 3,
        title: 'Mark Zuckerberg',
        description: 'Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.', 
        like: 1,
        dislike: 1,
        percent_like: 50,
        percent_dislike: 50,
        img: '../assets/imgs/mark.jpg', featured: 0},
      { id: 4,
        title: 'Cristina Fernández de Kirchner',
        description: 'Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.',
        like: 1,
        dislike: 1,
        percent_like: 50,
        percent_dislike: 50,
        img: '../assets/imgs/kristina.png', featured: 0},
      { id: 5,
        title: 'Malala Yousafzai',
        description: 'Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.',
        like: 1,
        dislike: 1,
        percent_like: 50,
        percent_dislike: 50,
        img: '../assets/imgs/malala.jpg', featured: 0}

    ];
    localStorage.setItem('people', JSON.stringify(people));
  }

  updateData(people) {
    localStorage.setItem('people', JSON.stringify(people));
  }
}
